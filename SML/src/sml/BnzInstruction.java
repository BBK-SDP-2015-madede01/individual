package sml;

public class BnzInstruction extends Instruction{
	
	private int register;
	private String labeledL2;

	public BnzInstruction(String label, String opcode) {
		super(label, opcode);
	}

	public BnzInstruction(String label, int register, String L2) {
		super(label, "bnz");
		this.register = register;
		this.labeledL2 = L2;
	}
	
	@Override
	public void execute(Machine m) {
		if(m.getRegisters().getRegister(register) !=0){
			
			m.setPc(m.getLabels().indexOf(labeledL2));
		}
		
	}
	
	@Override
	public String toString() {
		return super.toString() + " register " + register + " label is " + labeledL2;
	}

}
